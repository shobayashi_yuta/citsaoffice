require 'csv'
CSV.generate(encoding: Encoding::SJIS, row_sep: "\r\n", force_quotes: true) do |csv|
    csv_column_values = [
	"id",
	"uid",
	"date",
	"starttime",
	"finishtime",
	"department",
	"year",
	"sex",
	"contents",
	"remarks",
	"number",
	"created_at",
	"updated_at"
]
    csv << csv_column_values
  
	@users.each do |user|
		csv_values = [
			user.id,
			user.uid,
			user.date,
			user.starttime.strftime("%H:%M"),
			user.finishtime.strftime("%H:%M"),
			user.department,
			user.year,
			user.sex,
			user.contents,
			user.remarks,
			user.number,
			user.created_at,
			user.updated_at
		]
    csv << csv_values
  	end
end
