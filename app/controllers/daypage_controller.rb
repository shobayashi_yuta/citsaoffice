class DaypageController < ApplicationController
  def show
	require "date"
	@date = params[:date]
	@users = User.where("date = ?",params[:date]).order(:starttime)
	@nowTime = DateTime.now
	@department = Department.pluck(:name,:key)
  end

  def create
	@user = User.new()
	@user.date = params[:come][:date]
	@user.starttime = params[:come][:starttime]
	@user.finishtime = params[:come][:finishtime]
	@user.department = params[:come][:department]
	@user.year = params[:come][:year]
	@user.sex = params[:come][:sex]
	@user.contents = params[:come][:contents]
	@user.remarks = params[:come][:remarks]
	@user.number = params[:come][:number]
	@user.save
	redirect_to daypage_show_path(date: params[:come][:date])
	#redirect_to :show(date: params[:come][:date])
  end

  def delete
	@date = params[:date]
	User.find(params[:id]).destroy
	redirect_to daypage_show_path(date: @date)
  end
	

end
