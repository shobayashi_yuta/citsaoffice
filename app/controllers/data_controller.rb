class DataController < ApplicationController
  
  def new
    if params[:year].present? then
      @year = params[:year]
    else
      @year = ""
    end
    if params[:depart].present? then
      @depart = params[:depart]
    else
      @depart = ""
    end
    @users = User.all
    @department = Department.pluck(:key)
    end
  

  def delete
    if params[:year].present? then
      @year = params[:year]
    else
      @year = ""
    end
    if params[:depart].present? then
      @depart = params[:depart]
    else
      @depart = ""
    end
    User.find(params[:id]).destroy
    redirect_to data_new_path(year: @year,depart:@depart)
  end

  def search
    @department = Department.pluck(:key)
    
    if params[:come][:year].present? then
      @search_date = Date.new(params[:come][:year].to_i, 4, 1)
      @finish_date = Date.new(params[:come][:year].to_i + 1, 3, 31)
      #@users = User.where('date like ? ',"%#{params[:come][:year]}%")
      @users = User.where(date: @search_date..@finish_date)
      @year = params[:come][:year]
    else
      @users = User.all
      @year = ""
    end
    if params[:come][:department].present? then
      @users = @users.where("department = ?",params[:come][:department])
      @depart = params[:come][:department]
    else
      @depart = ""
    end

    render "new"
    
  end

  def csv
    #@users = User.all
    if params[:year].present? then
      @search_date = Date.new(params[:year].to_i, 4, 1)
      @finish_date = Date.new(params[:year].to_i + 1, 3, 31)
      #@users = User.where('date like ? ',"%#{params[:come][:year]}%")
      @users = User.where(date: @search_date..@finish_date)
    else
      @users = User.all
    end
    if params[:department].present? then
      @users = @users.where("department = ?",params[:department])
    end
  end
  
end
