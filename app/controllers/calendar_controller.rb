class CalendarController < ApplicationController
require "date"
require "csv"
   	def index
	  @users = User.all
	  @departments = Department.pluck(:key)
	  #@datas = ["機工","機電","先端","電電","情通","応化","建築","都環","デ科","未ロボ","生命","知能","情報","情ネ","経営","PM","金融"]
	  @count = []
	  @count_person = []
	  if params[:start_date].nil? then
		  @date = Date.today
	  else
		  @date = Date.strptime(params[:start_date])
	  end

	  @data = @users.where(date: @date.beginning_of_month..@date.end_of_month)  
	  @departments.each do |d|
		  @count.push(@data.where(department: d).count)
	  end
	  @departments.each do |d|
		  @count_person.push(@data.where(department: d).sum(:number))
	  end
	  @memos = @data.where.not(remarks: '').order("date DESC").limit(5)
  end
end
