class DepartmentController < ApplicationController
  def edit
	  @data = Department.all
  end

  def delete
	  Department.find(params[:id]).destroy
	  redirect_to department_edit_path
  end

  def create
	  @department = Department.new()
	  @department.name = params[:come][:name]
	  @department.key = params[:come][:key]
	  @department.save
	  redirect_to department_edit_path
  end
end
