Rails.application.routes.draw do
  get 'data/new'
  post 'data/delete'
  post 'data/search'
  get 'data/csv'
  get 'department/edit'

  get 'daypage/show'
  post 'daypage/create'
  post 'daypage/delete'
  get 'calendar/index'
  root 'calendar#index'
  post 'department/delete'
  post 'department/create'
  
  #root 'sample#indexcd'
  #root 'simple_calendar#_calendar'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

end
