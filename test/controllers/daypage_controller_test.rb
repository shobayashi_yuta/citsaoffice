require 'test_helper'

class DaypageControllerTest < ActionDispatch::IntegrationTest
  test "should get show" do
    get daypage_show_url
    assert_response :success
  end

end
