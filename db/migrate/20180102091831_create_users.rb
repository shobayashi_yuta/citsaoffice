class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.integer :uid
      t.date :date
      t.time :starttime
      t.time :finishtime
      t.string :department
      t.integer :year
      t.string :sex
      t.text :contents
      t.text :remarks
      t.integer :number

      t.timestamps
    end
  end
end
