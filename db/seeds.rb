# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

	Department.create(:name => '械機工学科',:key => '機工')
	Department.create(:name => '機械電子創成工学科',:key => '機電')
	Department.create(:name => '先端材料工学科',:key => '先端')
	Department.create(:name => '電気電子工学科',:key => '電電')
	Department.create(:name => '情報通信システム工学科',:key => '情通')
	Department.create(:name => '応用化学科',:key => '応化')
	Department.create(:name => '建築学科',:key => '建築')
	Department.create(:name => '都市環境工学科',:key => '都環')
	Department.create(:name => 'デザイン科学科',:key => 'デ科')
	Department.create(:name => '未来ロボティクス学科',:key => '未ロボ')
	Department.create(:name => '生命科学科',:key => '生命')
	Department.create(:name => '知能メディア工学科',:key => '知能')
	Department.create(:name => '情報工学科',:key => '情報')
	Department.create(:name => '情報ネットワーク学科',:key => '情ネ')
	Department.create(:name => '経営情報科学科',:key => '経営')
	Department.create(:name => 'プロジェクトマネジメント学科',:key => 'PM')
	Department.create(:name => '金融・リスク科学科',:key => '金融')

